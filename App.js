import React, { useCallback, useState, useEffect, useRef } from "react";
import { NavigationContainer } from "@react-navigation/native";
import * as SplashScreen from 'expo-splash-screen';
import navigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";
import OfflineNotice from "./app/components/OfflineNotice";
import AuthNavigator from "./app/navigation/AuthNavigator";
import AuthContext from "./app/auth/context";
import authStorage from "./app/auth/storage";
import navigationRef from "./app/navigation/rootNavigation";
import refreshApi from "./app/api/refresh";
import * as Notifications from 'expo-notifications';
import routes from "./app/navigation/routes";
import { RootSiblingParent } from 'react-native-root-siblings';
import * as Sentry from "@sentry/react-native";
import useNotifications from "./app/hooks/useNotifications";
import Constants from 'expo-constants';
import { StationProvider } from "./app/auth/stationContext";
import { useRingerMode, RINGER_MODE } from 'react-native-ringer-mode';
import { Platform } from "react-native";

const { sentryDsn } = Constants.expoConfig.extra;

Sentry.init({
  dsn: sentryDsn,
  tracesSampleRate: 1.0,
  _experiments: {
    // profilesSampleRate is relative to tracesSampleRate.
    // Here, we'll capture profiles for 100% of transactions.
    profilesSampleRate: 1.0,
  },
});

SplashScreen.preventAutoHideAsync();

function App() {
  const [user, setUser] = useState();
  const [isReady, setIsReady] = useState(false);
  const [forceRefreshUser, setForceRefreshUser] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const navRef = navigationRef;
  const { mode, error, setMode } = useRingerMode();

  useNotifications(false, false); // dont register token and register category here (will be done in app navigator)

  useEffect( () => {

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      notificationCommonHandler(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification 
    // (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      notificationCommonHandler(response.notification);
      //console.log("RESPONSE **** ", response);
      let alertData = response.notification.request.content.data; // EXPO and Android Payload
      if (!alertData && response.notification.request.trigger.payload.data) {
        alertData = response.notification.request.trigger.payload.data; // APN Payload
      }
      notificationNavigationHandler(alertData, response.actionIdentifier);
    });

    // The listeners must be clear on app unmount
    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  const notificationCommonHandler = (notification) => {
    // save the notification to reac-redux store
    //console.log('A notification has been received', notification);
    //console.log('A notification has been received - payload', notification.request.trigger.payload);
    //ToastAndroid.show('Notification received successfully!', ToastAndroid.SHORT);
    //TODO: send ping that notification was received to allow sms sendout of not
    
    // Set Ringer to MAX Volume
    if (Platform.OS === 'android') {
      setMode(RINGER_MODE.normal);
    }
  }


  const notificationNavigationHandler = (alertData, actionIdentifier) => {
    // navigate to app screen
    console.log('A notification has been touched', alertData);
    //ToastAndroid.show('A notification has been touched', ToastAndroid.SHORT);
    if (alertData && alertData.screen === routes.ALERTS) {
      navRef.navigate(routes.ALERTS_NAVIGATOR, { screen: routes.ALERTS });
    }
    if (alertData && alertData.screen === routes.ALERT_DETAILS) {
      if (alertData.alert) {
        navRef.navigate(routes.ALERTS_NAVIGATOR, { screen: routes.ALERT_DETAILS, params: { alert: alertData.alert, actionIdentifier: actionIdentifier } });
      } else {
        navRef.navigate(routes.ALERTS_NAVIGATOR, { screen: routes.ALERTS });
      }
    }
  }

  const restoreUser = async () => {

    // check if there is a token
    const decrypted_user = await authStorage.getUser();
    let firemonUser = null;
    let firemonStation = null;
    let firemonLinkedStations = null;
    let token = null;
    // token not existing? -> login
    if (!decrypted_user) {
      console.log("no token found. Please Login using form");
      setUser(null);
      return;
    }

    let needsRefresh = false;
    if (forceRefreshUser) {
      needsRefresh = true;
    } else {
      // check if the token is not expired within 14d
      console.log("Token Exp: ", new Date(decrypted_user.exp * 1000).toISOString());
      if (decrypted_user.exp * 1000 < Date.now() + (60*60*24*7*1000)) {
          console.log("token will expire. Refresh.");
          needsRefresh = true;
      } else {
        firemonUser = await authStorage.getFiremonUser();
        firemonStation = await authStorage.getFiremonStation();
        firemonLinkedStations = await authStorage.getFiremonLinkedStations();
        token = await authStorage.getToken();
        // check if there is objects
        if (!firemonUser || !firemonStation || !firemonLinkedStations) {
          // if not all objects -> refresh
          console.log("firemonUser or firemonStation missing. Refresh.");
          needsRefresh = true;
        }
      }
    }
    
    if (needsRefresh) {
      console.log("Refreshing decrypted_user...", decrypted_user);
      const response = await refreshApi.refresh();
      if (response == null || !response.ok || response.data == null || response.data.data == null) {
        Sentry.captureException(new Error("RefreshApi failed: " + response.problem));
        setUser(null);
        return;
      }
      firemonUser = response.data.data.user;
      firemonStation = response.data.data.station;
      firemonLinkedStations = response.data.data.linked_stations;
      token = response.data.data.token;
      console.log("Firemon User after Refresh", firemonUser);
      console.log("Firemon Station after Refresh", firemonStation);
      console.log("Firemon Linked Station after Refresh", firemonLinkedStations);
    }
    
    if (token !== null && decrypted_user != null && firemonUser != null && decrypted_user.sub == firemonUser.id && firemonUser.id > 0 && firemonUser.allow_app_usage == 1) {
      console.log("Login accepted. App-User:", { ...decrypted_user, ...firemonUser, ...{station: firemonStation }, ...{linked_stations: firemonLinkedStations} });
      await authStorage.storeToken(token);
      await authStorage.storeFiremonUser(firemonUser);
      await authStorage.storeFiremonStation(firemonStation);
      await authStorage.storeFiremonLinkedStations(firemonLinkedStations);
      setUser({ ...decrypted_user, ...firemonUser, ...{station: firemonStation }, ...{linked_stations: firemonLinkedStations} });
    } else {
      console.log("Login not accepted");
      await authStorage.removeFiremonLinkedStations();
      await authStorage.removeFiremonStation();
      await authStorage.removeFiremonUser();
      await authStorage.removeToken();
      setUser(null);
    }
  };

  

  useEffect(() => {
    if (forceRefreshUser) {
      console.log("ForceRestoreUser");
      restoreUser();
      setForceRefreshUser(false);
    }
  },[forceRefreshUser]);


  useEffect(() =>  {
    console.log("RestoreUser");
    restoreUser();
    SplashScreen.hideAsync();
    setIsReady(true);
    if (user) {
      navRef.handlePendingNavigation();
    }
  },[]);

  console.log("app lauching...");
  
  return (
    <RootSiblingParent>
      <AuthContext.Provider value={{ user, setUser, forceRefreshUser, setForceRefreshUser }}>
        <OfflineNotice />
        <NavigationContainer ref={navRef.navigationRef} theme={navigationTheme} >
          {user ? (
            <StationProvider>
              <AppNavigator />
            </StationProvider>
          ) : (
            <AuthNavigator />
          )}
        </NavigationContainer>
      </AuthContext.Provider>
    </RootSiblingParent>
  );
}

export default Sentry.wrap(App);
