import 'dotenv/config';

export default ({ config }) => {
    return {
        ...config,
        extra: {
            ...config.extra,
            sentryDsn: process.env.SENTRY_DSN,
            eas: {
                projectId: "ceda8a7c-38d7-4cd0-aa46-7496204f4d9a"
            }
        }
    };
};