## Environment Variables Setup

To run this project, you'll need to set up the following environment variables:

- `SENTRY_DSN`: Used for Logging and Exception Reporting. Also provide eas secret this env to get build running

Refer to our `env.sample` file for a template of required environment variables. Copy this file as `.env` in your local setup and replace the placeholder values with your specific configurations.

## Project Setup

Note that you need your own expo.dev account and project - you need to change projectId in `app.config.js` and all other project related information like name, slug etc in the static `app.json` (`app.json` gets parsed and combined with dynamic properties in `app.config.js`)
