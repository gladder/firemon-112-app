import Constants from "expo-constants";
import * as Device from 'expo-device';

const settings = {
  devEmu: {
    apiUrl: "http://10.0.2.2/jwt/",
  },
  devRealDevice: {
    apiUrl: "http://192.168.179.50/jwt/",
  },
  staging: {
    apiUrl: "https://stage.firemon112.de/jwt/",
  },
  prod: {
    apiUrl: "https://www.firemon112.de/jwt/",
  },
};

const monitorUrl = {
  devEmu: {
    monitorUrl: "http://10.0.2.2/monitor",
  },
  devRealDevice: {
    monitorUrl: "http://192.168.179.50/monitor",
  },
  staging: {
    monitorUrl: "https://stage.firemon112.de/monitor",
  },
  prod: {
    monitorUrl: "https://www.firemon112.de/monitor",
  },
};

const getCurrentSettings = () => {
  if (__DEV__)  {
    if (Device.isDevice) {
      return settings.devRealDevice;
    }
    return settings.devEmu;
  }
  if (Constants.manifest.releaseChannel === "staging") return settings.staging;
  return settings.prod;
};

const getCurrentMonitorUrl = () => {
  if (__DEV__)  {
    if (Device.isDevice) {
      return monitorUrl.devRealDevice;
    }
    return monitorUrl.devEmu;
  }
  if (Constants.manifest.releaseChannel === "staging") return monitorUrl.staging;
  return monitorUrl.prod;
};

export default { getCurrentSettings, getCurrentMonitorUrl };
