import { useContext } from "react";
import jwtDecode from "jwt-decode";

import AuthContext from "./context";
import authStorage from "./storage";


export default useAuth = () => {
  const { user, setUser } = useContext(AuthContext);

  const logIn = async (response) => {
    console.log("call auth api");
    if (response.data && response.data.token && response.data.user) {
      const decrypted_user = jwtDecode(response.data.token);
      const login_user = { ...decrypted_user, ...response.data.user, ...{station: response.data.station}, ...{linked_stations: response.data.linked_stations} }
      //console.log("Login User after Login", login_user);
      if (login_user.sub == login_user.id && login_user.id > 0 && login_user.allow_app_usage == 1) {
        //console.log("auth response", response.data);
        //console.log("store token");
        await authStorage.storeToken(response.data.token);
        await authStorage.storeFiremonUser(response.data.user);
        await authStorage.storeFiremonStation(response.data.station);
        await authStorage.storeFiremonLinkedStations(response.data.linked_stations);
        //console.log("stored token",response.data.token);
        setUser(login_user);
        return true;
      }
    }
    setUser(null);
    return false;
  };

  const logOut = () => {
    setUser(null);
    authStorage.removeToken();
    authStorage.removeFiremonUser();
    authStorage.removeFiremonStation();
    authStorage.removeFiremonLinkedStations();
  };

  const refresh = (token, firemonUser, firemonStation, firemonLinkedStations) => {
    authStorage.storeToken(token);
    authStorage.storeFiremonUser(firemonUser);
    authStorage.storeFiremonStation(firemonStation);
    authStorage.storeFiremonLinkedStations(firemonLinkedStations);
  };

  return { user, logIn, logOut, refresh };
};
