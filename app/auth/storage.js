import * as SecureStore from "expo-secure-store";
import jwtDecode from "jwt-decode";

const key = "authToken";
const firemonUserKey = "firemonUser";
const firemonStationKey = "firemonStation";
const firemonLinkedStationsKey = "firemonLinkedStations";

const storeToken = async (authToken) => {
  try {
    await SecureStore.setItemAsync(key, authToken);
  } catch (error) {
    console.log("Error storing the auth token", error);
  }
};

const getToken = async () => {
  try {
    return await SecureStore.getItemAsync(key);
  } catch (error) {
    console.log("Error getting the auth token", error);
  }
};

const storeFiremonUser = async (firemonUser) => {
  try {
    //console.log("Stringified user: ", JSON.stringify(firemonUser));
    await SecureStore.setItemAsync(firemonUserKey, JSON.stringify(firemonUser));
  } catch (error) {
    console.log("Error storing the firemon user", error);
  }
};

const getFiremonUser = async () => {
  try {
    let json = await SecureStore.getItemAsync(firemonUserKey);
    return JSON.parse(json);
  } catch (error) {
    console.log("Error getting the firemon user", error);
    return null;
  }
};

const removeFiremonUser = async () => {
  try {
    await SecureStore.deleteItemAsync(firemonUserKey);
  } catch (error) {
    console.log("Error removing the firemon user", error);
  }
};

const storeFiremonStation = async (firemonStation) => {
  try {
    await SecureStore.setItemAsync(firemonStationKey, JSON.stringify(firemonStation));
  } catch (error) {
    console.log("Error storing the firemon station", error);
  }
};

const getFiremonStation = async () => {
  try {
    let json = await SecureStore.getItemAsync(firemonStationKey);
    return JSON.parse(json);
  } catch (error) {
    console.log("Error getting the firemon station", error);
    return null;
  }
};

const removeFiremonStation = async () => {
  try {
    await SecureStore.deleteItemAsync(firemonStationKey);
  } catch (error) {
    console.log("Error removing the firemon station", error);
  }
};

const storeFiremonLinkedStations = async (firemonLinkedStations) => {
  try {
    await SecureStore.setItemAsync(firemonLinkedStationsKey, JSON.stringify(firemonLinkedStations));
  } catch (error) {
    console.log("Error storing the firemon linked stations", error);
  }
};

const getFiremonLinkedStations = async () => {
  try {
    let json = await SecureStore.getItemAsync(firemonLinkedStationsKey);
    return JSON.parse(json);
  } catch (error) {
    console.log("Error getting the firemon linked stations", error);
    return null;
  }
};

const removeFiremonLinkedStations = async () => {
  try {
    await SecureStore.deleteItemAsync(firemonLinkedStationsKey);
  } catch (error) {
    console.log("Error removing the firemon linked stations", error);
  }
};

const getUser = async () => {
  const token = await getToken();
  return token ? jwtDecode(token) : null;
};

const removeToken = async () => {
  try {
    await SecureStore.deleteItemAsync(key);
  } catch (error) {
    console.log("Error removing the auth token", error);
  }
};

export default { 
  getToken, 
  getUser, 
  removeToken, 
  storeToken, 
  storeFiremonStation, 
  storeFiremonLinkedStations, 
  storeFiremonUser, 
  getFiremonStation, 
  getFiremonLinkedStations,
  getFiremonUser, 
  removeFiremonStation,
  removeFiremonLinkedStations,
  removeFiremonUser
};
