import React, { createContext, useState, useContext, useEffect } from 'react';
import authStorage from './storage';

const StationContext = createContext();

export function StationProvider({ children }) {
    const [stations, setStations] = useState([]);
    const [selectedStation, setSelectedStation] = useState(null);

    
    const loadStations = async () => {
        const station = await authStorage.getFiremonStation();
        const linkedStations = await authStorage.getFiremonLinkedStations();
        let stations = [];
        if (station !== null) {
            stations.push(station);
        }
        if (linkedStations && linkedStations.length > 0) {
            stations.push(...linkedStations);
        }
        setStations(stations);
        if (stations && stations.length > 0) {
            setSelectedStation(stations[0]);
        }
    };

    useEffect(() => {
        loadStations();
    }, []);

    return (
        <StationContext.Provider value={{ stations, selectedStation, setSelectedStation }}>
            {children}
        </StationContext.Provider>
    );
}

export function useStation() {
    return useContext(StationContext);
}
