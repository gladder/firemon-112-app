import React, {useState} from "react";
import { View, TextInput, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import defaultStyles from "../config/styles";

function AppTextInput({ icon, width = "100%", ...otherProps }) {
  const [hidePassword, setHidePassword] = useState(true);
  const isPasswordField = otherProps.secureTextEntry;
  if (isPasswordField && !hidePassword) {
    otherProps.secureTextEntry = false;
  }
  return (
    <View style={[styles.container, { width }]}>
      {icon && (
        <MaterialCommunityIcons
          name={icon}
          size={32}
          color={defaultStyles.colors.medium}
          style={styles.icon}
        />
      )}
      <TextInput
        placeholderTextColor={defaultStyles.colors.medium}
        style={[defaultStyles.text, styles.textBox]}
        {...otherProps}
      />
      {isPasswordField && (
        <MaterialCommunityIcons
          name={hidePassword ? "eye-off" : "eye"} 
          size={32}
          color={defaultStyles.colors.medium}
          style={styles.icon}
          onPress={() => setHidePassword(!hidePassword) }
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: defaultStyles.colors.light,
    borderRadius: 25,
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
  },
  icon: {
    marginRight: 10,
  },
  textBox: {
    flex: 1,
  }
});

export default AppTextInput;
