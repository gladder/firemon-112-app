import React from "react";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";
import FeedbackButton from "./FeedbackButton";
import moment from 'moment';
import useAuth from "../auth/useAuth";
import Text from "./Text";
import colors from "../config/colors";
import alertFeedback from "../api/alertFeedback";
function AlertCard({ alert, onPress, onFeedback }) {
  const { user } = useAuth();
  const alertDt = moment(alert.created_at);
  const active = moment().subtract(2, 'hours') < alertDt;
  const getAlertFeedbackApi = useApi(alertFeedback.feedback);

  const onFeedbackPress = async (alert_id, feedback) => {
    await getAlertFeedbackApi.request(alert_id, feedback);
    onFeedback();
  }

  let station = user.station; // Old Handling - keep for legacy
  if (alert.station !== null) {
    station = alert.station; // New Handling considers station that got alert (Zweitwehr usw)
  }
  let testAlarm = false;
  if (alert.test !== null) {
    testAlarm = alert.test;
  }

  let containersStyle = styles.detailsContainer;
  if (active) {
    if (testAlarm) {
      containersStyle = StyleSheet.compose(styles.detailsContainer, styles.detailsContainerTestAlert);
    } else {
      containersStyle = StyleSheet.compose(styles.detailsContainer, styles.detailsContainerActiveAlert);
    }
  }

  let showFeedbackSummary = false;
  if (alert.mission_admin != null && alert.mission_operator != null) {
    showFeedbackSummary = alert.mission_admin || alert.mission_operator;
  }

  return (
    <TouchableWithoutFeedback onPress={onPress} onFeedback={onFeedback}>
      <View style={ containersStyle }>
        <View style={styles.headline}>
          <Text style={ (active ? styles.titleActive : styles.title) } numberOfLines={1}>
            {alert.Einsatzstichwort + (active ? ' (Aktiv)' : '') }
          </Text>
          <Text style={styles.date} numberOfLines={1}>
            {alertDt.format("D.M.YYYY HH:mm")}
          </Text>
        </View>
        {testAlarm && (
          <Text style={ styles.testNote }>
            Test-Alarm!
          </Text>
        )}
        <Text style={styles.subTitle} numberOfLines={2}>
          {alert.Information}
        </Text>
        <Text style={ (active ? styles.textActive : styles.text) }>
          {alert.Wehr}
        </Text>
        {active && station.alert_feedback_type > 0 && (
        <>
        <View style={styles.feedbackHolder}>
          <View style={styles.feedbackHolderRow}>
            <FeedbackButton
              title="Sofort"
              onPress={() => onFeedbackPress(alert.id, 1)}
              color="feedback_instant"
              icon="lightning-bolt"
              state={ alert.feedback?.feedback_status === 1 }
            />
            <FeedbackButton
              title="Nicht Verfügbar"
              onPress={() => onFeedbackPress(alert.id, 4)}
              color="feedback_notavail"
              icon="close-circle"
              state={ alert.feedback?.feedback_status === 4 }
            />
          </View>
          {(station.alert_feedback_type > 1) && (
          <View style={styles.feedbackHolderRow}>
            {(station.alert_feedback_type == 2 || station.alert_feedback_type == 4) && (
            <FeedbackButton
              title="Verspätet"
              onPress={() => onFeedbackPress(alert.id, 3)}
              color="feedback_later"
              icon="timer-sand"
              state={ alert.feedback?.feedback_status === 3 }
            />
            )}
            {(station.alert_feedback_type == 3 || station.alert_feedback_type == 4) && (
            <FeedbackButton
              title="Direkt"
              onPress={() => onFeedbackPress(alert.id, 2)}
              color="feedback_direct"
              icon="map-marker"
              state={ alert.feedback?.feedback_status === 2 }
            />
            )}
          </View>
          )}
          {showFeedbackSummary && alert.feedback_summary && alert.feedback_summary.alert_feedback_types && alert.feedback_summary.alert_feedback_types.length > 0 && (
            <View style={styles.feedbackHolderRow}>
              {alert.feedback_summary.alert_feedback_types.map((feedback, index) => (
                <View style={[styles.feedbackTextContainer, getBackgroundColor(feedback.type)]}>
                  <Text key={index} style={styles.feedbackText}>{feedback.count}</Text>
                </View>
              ))}
            </View>
          )}
        </View>
        </>
      )}
      </View>
    </TouchableWithoutFeedback>
  );
}

const getBackgroundColor = (flag) => {
  switch(flag) {
    case "alert-feedback-i":
      return { backgroundColor: colors.feedback_instant };
    case "alert-feedback-d":
      return { backgroundColor: colors.feedback_direct };
    case "alert-feedback-l":
      return { backgroundColor: colors.feedback_later };
    case "alert-feedback-n":
      return { backgroundColor: colors.feedback_notavail };
    default:
      return { backgroundColor: colors.dark };
  }
};

const styles = StyleSheet.create({
  feedbackHolder: {
    marginHorizontal: -5,
    marginBottom: -5,
    marginTop: 5,
    backgroundColor: "white",
    borderRadius: 10,
  },
  feedbackHolderRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsContainer: {
    borderRadius: 15,
    backgroundColor: colors.white,
    marginBottom: 20,
    overflow: "hidden",
    padding: 20,
  },
  detailsContainerActiveAlert: {
    backgroundColor: colors.primary,
  },
  detailsContainerTestAlert: {
    backgroundColor: colors.mediumlight,
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
  },
  testNote: {
    color: colors.green,
    fontWeight: "bold",
  },
  text: {
    color: colors.medium,
  },
  textActive: {
    color: colors.white
  },
  headline: {
    display: "flex",
    flexDirection: "row"
  },
  title: {
    color: colors.primary,
    marginBottom: 7,
    fontWeight: "bold",
    flex: 1
  },
  titleActive: {
    color: colors.white,
    marginBottom: 7,
    fontWeight: "bold",
    flex: 1
  },
  date: {
    color: colors.dark,
    marginBottom: 7,
    fontWeight: "bold",
    flex: 1,
    textAlign: "right"
  },
  feedbackTextContainer: {
    padding: 5,
    margin: 5,
    borderRadius: 10,
    flex: 1,
  },
  feedbackText: {
    color: '#333',
    color: colors.white,
    fontSize: 26,
    textAlign: 'center'
  }
});

export default AlertCard;
