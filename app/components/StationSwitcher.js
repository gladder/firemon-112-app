import React, { useState, useEffect  } from 'react';
import { View, StyleSheet } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { useStation } from '../auth/stationContext'; // Ensure path is correct
import colors from '../config/colors';

const StationSwitcher = () => {
  const { stations, selectedStation, setSelectedStation } = useStation();
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(selectedStation ? selectedStation.id : null);
  const [items, setItems] = useState(stations.map(station => ({
    label: station.name,
    value: station.id
  })));

  useEffect(() => {
    if (selectedStation.id != value) {
        setValue(selectedStation.id);
    }
}, [selectedStation]);

  // Update local value and selected station on change
  const onValueChange = (itemValue) => {
    const newSelectedStation = stations.find(station => station.id === itemValue);
    if (newSelectedStation.id != selectedStation.id) {
      setSelectedStation(newSelectedStation);
    }
  };

  return (
    <View style={styles.container}>
      <DropDownPicker
        open={open}
        value={value}
        items={items}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setItems}
        onChangeValue={onValueChange}
        style={styles.picker}
        dropDownContainerStyle={styles.dropdownContainer}
        listItemLabelStyle={styles.listItemLabel}
        labelStyle={styles.label}
        placeholder="Organisation wählen"
        zIndex={3000} // Ensure dropdown is above other components
        zIndexInverse={1000}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 0,
    backgroundColor: colors.red,
    justifyContent: 'center',
    width: 250,
    padding: 0,
    margin: 0,
  },
  picker: {
    backgroundColor: colors.white,
    borderColor: colors.dark,
    borderWidth: 0,
    minHeight: 30,
    borderBottomWidth: 1,
    borderRadius: 0
  },
  dropdownContainer: {
    backgroundColor: colors.light,
    borderColor: colors.mediumlight
  },
  label: {
    fontWeight: 'bold',
    color: colors.primary
  }
});

export default StationSwitcher;
