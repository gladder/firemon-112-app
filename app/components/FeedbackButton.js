import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import colors from "../config/colors";
import RawIcon from "./RawIcon";

function FeedbackButton({ title, onPress, color, icon, state }) {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: state ? colors[color] : colors["white"] }, { borderColor: colors[color] }]}
      onPress={onPress}
    >
      <RawIcon name={icon} iconColor={state ? colors["white"] : colors[color]} />
      <View style={styles.textContainer}>
        <Text style={[styles.text, { color: state ? colors["white"] : colors[color] }]}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: "center",
    padding: 10,
    borderRadius: 10,
    borderWidth: 2,
    margin: 5,
  },
  text: {
    fontSize: 12,
    textTransform: "uppercase",
    textAlign: 'center',
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default FeedbackButton;
