import React, { useState, useContext } from "react";
import { StyleSheet, View } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import * as Yup from "yup";
import { Platform } from "react-native";

import Screen from "../components/Screen";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import account from "../api/account";
import AppText from "../components/Text";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import ActivityIndicator from "../components/ActivityIndicator";

const validationSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, "Das Passwort muss mindestens 11 Zeichen lang sein")
    .matches(/(?=.*[0-9])/, "Das Passwort muss mindestens eine Zahl beinhalten")
    .matches(/(?=.*[A-Z])/, "Das Passwort muss mindestens ein Großbuchstaben beinhalten")
    .required("Angabe notwendig"),
  password_confirmation: Yup.string()
    .oneOf([Yup.ref("password"), null], "Passwörter müssen übereinstimmen")
    .required("Angabe notwendig"),
});

function AccountPasswordScreen({ navigation }) {
  const { user, logOut } = useAuth();
  const passwordApi = useApi(account.password);
  const [error, setError] = useState();
  const [success, setSuccess] = useState(false);

  const handleSubmit = async ({ password, password_confirmation }) => {
    console.log("submit");
    const result = await passwordApi.request( password, password_confirmation );

    if (!result || !result.ok || !result.data || !result.data.success) {
      if (result.data && result.data.error) {
        setError(result.data.error);
      } else {
        setError("Ein unerwarteter Fehler ist aufgetreten");
        console.log(result);
      }
      return;
    } else {
      logOut();
    }
  };

  return (
    <>
      <ActivityIndicator visible={passwordApi.loading} />
      <Screen style={styles.container}>
        <KeyboardAwareScrollView
          behavior="position"
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
          keyboardShouldPersistTaps="true"
        >
          <View>
          <AppText>Nachdem das Passwort erfolgreich geändert wurde wirst du automatisch abgemeldet und musst dich mit dem neuen Passwort einloggen.</AppText>
          <Form
            initialValues={{ 
              password: '', 
              password_confirmation: '',
            }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <ErrorMessage error={error} visible={error} />
            <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password"
                placeholder="neues Passwort"
                secureTextEntry
                textContentType="password"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password_confirmation"
                placeholder="Passwort wiederholen"
                secureTextEntry
                textContentType="password"
              />
            <SubmitButton title="Passwort ändern" />
          </Form>
          </View>
          
        </KeyboardAwareScrollView>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default AccountPasswordScreen;
