import React, { useEffect, useState }  from "react";
import { View, StyleSheet, RefreshControl, FlatList, Text, TouchableOpacity, Modal } from "react-native";
import AppText from "../components/Text";
import Button from "../components/Button";
import news from "../api/news";
import ActivityIndicator from "../components/ActivityIndicator";
import colors from "../config/colors";
import moment from "moment";
import Screen from "../components/Screen";
import useApi from "../hooks/useApi";
import { Image } from "react-native-expo-image-cache";
import Svg, { SvgXml, Circle, Rect, Path } from 'react-native-svg';
import { useStation } from "../auth/stationContext";
import { useIsFocused } from "@react-navigation/native";

function NewsScreen({ navigation }) {
  const [items, setItems] = useState([]);
  const [error, setError] = useState(false);
  const getNewsApi = useApi(news.getNews);
  const [modalVisible, setModalVisible] = useState(false);
  const { selectedStation } = useStation();
  const isFocused = useIsFocused();

  useEffect(() => {
    loadNews();
  }, [selectedStation, isFocused]);

  const loadNews = async () => {
    if (!selectedStation || !isFocused) {
      //console.log("STOPPED Calling News - no station or not in focus", selectedStation === null, isFocused);
      return;
    }
    //console.log("PERFORM Call News");
    const resp = await getNewsApi.request({ station_id: selectedStation.id});
    let newItems = [];
    if (resp && resp.ok && resp.data && resp.data.success) {
      setItems(resp.data.data);
    } else{
      setError(true);
    }
    
  }

  const onRefresh = () => {
    loadNews();
  };

  _listEmptyComponent = () => {
    return (
      <AppText style={styles.emptyList}>Es sind keine News vohanden</AppText>
    );
  }

  return (
    <>
      <Screen style={styles.screen}>
        {error && (
          <>
            <AppText>Es konnten keine Daten empfangen werden.</AppText>
            <Button title="Erneut versuchen" onPress={onRefresh} />
          </>
        )}
        <FlatList
          refreshControl={
            <RefreshControl refreshing={getNewsApi.loading} onRefresh={onRefresh} />
          }
          style={styles.newsHolder}
          data={items}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => {
            const from = moment(item.visible_from);
            const to = moment(item.visible_to);
            //console.log(item.image);
            return (
              <View style={styles.item}>
                <View style={styles.itemHeadline}>
                  <SvgXml style={styles.svg} xml={item.category_svg} />
                  <Text style={styles.newsheadline}>{item.category_readable} ({from.format("DD.MM.YYYY")})</Text>
                </View>
                <View style={styles.itemDetails}>
                  <Text style={styles.details}>{item.content}</Text>
                  {item.image != null && (
                    <>
                      <Image
                        style={styles.image}
                        tint="light"
                        uri={item.image.path}
                      />
                    </>
                  )}
                </View>
              </View>
            );
          }}
          ListEmptyComponent={this._listEmptyComponent}
        />
      </Screen>
    </>
  );

}

const styles = StyleSheet.create({
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
  newsHolder: {
    backgroundColor: colors.white,
    borderRadius: 15,
    flex: 1,
    padding: 10,
  },
  emptyList: {
    textAlign: "center",
    paddingVertical: 20
  },
  item: {
    padding: 20,
    marginBottom: 10,
    backgroundColor: colors.light,
    borderRadius: 5,
    flexDirection: "column"
  },
 
  itemDetails: {
    flex: 1
  },
  itemHeadline: {
    flex: 1,
    flexDirection: "row"
  },
  newsheadline: {
    fontSize: 16
  },
  details: {
    fontSize: 24,
    fontWeight: "bold"
  },
  svg: {
    fill: colors.primary,
    width: 24,
    height: 24
  },
  image: {
    height: 200,
    width: "100%"
  },
});

export default NewsScreen;
