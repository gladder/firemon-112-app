import React, { useState, useContext } from "react";
import { StyleSheet, View } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import * as Yup from "yup";

import Screen from "../components/Screen";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import account from "../api/account";
import AuthContext from "../auth/context";
import { Platform } from "react-native";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import ActivityIndicator from "../components/ActivityIndicator";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Angabe notwendig").label("Vorname"),
  surname: Yup.string().required("Angabe notwendig").label("Nachname"),
  email: Yup.string().required("Angabe notwendig").email("Fehlerhaftes E-Mail Format").label("Account Email"),
  alert_sms: Yup.string()
    .matches(/^(\+491|491|00491|01){1}[0-9]*$/, "Falsches Rufnummerformat")
    .min(10, "Rufnummer zu kurz")
    .max(16, "Rufnummer zu lang")
    .required("Angabe notwendig")
    .label("Rufnummer für SMS"),
  alert_call: Yup.string()
    .matches(/^(\+49|49|0049|0){1}[0-9]*$/, "Falsches Rufnummerformat")
    .min(6, "Rufnummer zu kurz")
    .max(22, "Rufnummer zu lang")
    .nullable()
    .label("Alarm Rufnummer für Voice-Call"),
  alert_call_second: Yup.string()
    .matches(/^(\+49|49|0049|0){1}[0-9]*$/, "Falsches Rufnummerformat")
    .min(6, "Rufnummer zu kurz")
    .max(22, "Rufnummer zu lang")
    .nullable()
    .label("Zweite Alarm Rufnummer für Voice-Call"),
  alert_email: Yup.string().email("Fehlerhaftes E-Mail Format").label("Alarm Email"),
});

function AccountDataScreen({ navigation }) {
  const { user } = useAuth();
  const { forceRefreshUser, setForceRefreshUser } = useContext(AuthContext);
  const accountApi = useApi(account.account);
  const [error, setError] = useState();
  const [success, setSuccess] = useState(false);

  const handleSubmit = async ({ name, surname, email, alert_sms, alert_call, alert_call_second, alert_email }) => {
    console.log("submit");
    const result = await accountApi.request( name, surname, email, alert_sms, alert_call, alert_call_second, alert_email );

    if (!result || !result.ok || !result.data || !result.data.success) {
      if (result.data && result.data.error) {
        setError(result.data.error);
      } else {
        setError("Ein unerwarteter Fehler ist aufgetreten");
        console.log(result);
      }
      return;
    } else {
      console.log("ALL FINE");
      setSuccess(true);
      setForceRefreshUser(true);
      navigation.goBack();
    }
  };

  return (
    <>
      <ActivityIndicator visible={accountApi.loading} />
      <Screen style={styles.container}>
        <KeyboardAwareScrollView
          behavior="position"
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
          keyboardShouldPersistTaps="true"
        >
          <View>
          <Form
            initialValues={{ 
              name: user.name, 
              surname: user.surname, 
              email: user.email, 
              alert_sms: user.alert_sms,
              alert_call: user.alert_call,
              alert_call_second: user.alert_call_second,
              alert_email: user.alert_email
            }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <ErrorMessage error={error} visible={error} />
            <FormField
              autoCorrect={false}
              icon="account"
              name="name"
              placeholder="Vorname"
            />
            <FormField
              autoCorrect={false}
              icon="account"
              name="surname"
              placeholder="Nachname"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon="email"
              keyboardType="email-address"
              name="email"
              placeholder="E-Mail Adresse"
              textContentType="emailAddress"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon="cellphone"
              keyboardType="phone-pad"
              name="alert_sms"
              placeholder="Handynummer"
              textContentType="telephoneNumber"
            />
            {user.trigger_alert_call == 1 && 
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="phone-alert-outline"
                keyboardType="phone-pad"
                name="alert_call"
                placeholder="Alarm Rufnummer für Voice-Call"
                textContentType="telephoneNumber"
              />
            }
            {user.trigger_alert_call == 1 && 
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="phone-alert-outline"
                keyboardType="phone-pad"
                name="alert_call_second"
                placeholder="Zweite Alarm Rufnummer für Voice-Call"
                textContentType="telephoneNumber"
              />
            }
            {user.trigger_alert_email == 1 && 
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="alert_email"
                placeholder="Alarm E-Mail"
                textContentType="emailAddress"
              />
            }
            <SubmitButton title="Aktualisieren" />
          </Form>
          </View>
          
        </KeyboardAwareScrollView>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default AccountDataScreen;
