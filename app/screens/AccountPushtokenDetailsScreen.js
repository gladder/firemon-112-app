import React, { useEffect, useState } from "react";
import { Alert, StyleSheet, View, FlatList } from "react-native";
import { ListItem, ListItemSeparator } from "../components/lists";
import colors from "../config/colors";
import Icon from "../components/Icon";
import Screen from "../components/Screen";
import pushtoken from "../api/pushtoken";
import testPushToken from "../api/testPushToken";
import * as Notifications from 'expo-notifications';
import Toast from "react-native-root-toast";
import pushTokensApi from "../api/pushTokens";
import Constants from 'expo-constants';


function AccountPushtokenDetailsScreen({ route, navigation }) {
  const token = route.params.token;
  const testPushTokenApi = useApi(testPushToken.testPushToken);
  const [currentToken, setCurrentToken] = useState("");

  useEffect(() => {
    setToken();
  }, []);

  const setToken = async () => {
    let existingToken = null;
    if (__DEV__)  {
      existingToken = (await Notifications.getExpoPushTokenAsync({
        projectId: Constants.expoConfig.extra.eas.projectId,
      }));
    } else {
      existingToken = (await Notifications.getDevicePushTokenAsync());
    }
    if (existingToken != null) {
      setCurrentToken(existingToken.data);
    }
  };

  deleteToken = (item) => {
    Alert.alert('Token löschen', 'Token wirklich löschen? Versehentlich gelöschte Tokens werden beim nächsten App Start / Login automatisch neu erstellt.', [
      {
        text: 'Ja, löschen', onPress: ( () => {
          performDeleteToken(item);
        })
      },
      {
        text: 'Abbrechen',
        style: 'destructive'
      },
    ]);
  };

  performDeleteToken = (item) => {
    console.log("del token");
    pushTokensApi.deleteToken(item.token, item.token_type, item.device_name);
    setTimeout(() => {
      // give server some time to persist delete and then go back to list where tokenlist will be refreshed
      navigation.goBack();
    }, 1500);
  }

  triggerNotification = (item) => {
    Alert.alert('Test-Push', 'Hiermit wird eine Test Notification für Gerät '+item.device_name+' abgesetzt. Wann soll das erfolgen?', [
      {
        text: 'Sofort', onPress: ( () => {
          performNotification(item, 0);
        })
      },
      {
        text: '1 Minute', onPress: ( () => {
          performNotification(item, 1);
        })
      },
      {
        text: '3 Minuten', onPress: ( () => {
          performNotification(item, 3);
        })
      },
      {
        text: 'Abbrechen',
        style: 'destructive'
      },
    ]);
    
  }

  performNotification = (item, delay_minutes) => {
    testPushTokenApi.request(item.id, delay_minutes);
    let delay_hint = 'Sofort';
    if (delay_minutes > 0) {
      delay_hint = 'in ' + delay_minutes + ' Minuten';
    }
    Toast.show('Test-Push abgesetzt für ' + item.device_name + ' ('+delay_hint+')', {
      duration: Toast.durations.SHORT,
      position: Toast.positions.CENTER
    });
  }

  let menuItems = [
    {
      title: token.device_name + (currentToken == token.token ? " (dieses Gerät)" : ""),
      subTitle: "Test-Notification auslösen",
      icon: {
        name: "bell-ring",
        backgroundColor: colors.primary,
      },
      onPressCallback: () => triggerNotification(token)
    },
    {
      title: "Typ: " + token.token_type,
      subTitle: "Erstellt: " + token.updated_at,
      icon: {
        name: "information",
        backgroundColor: colors.secondary,
      },
    }
  ];

  if (currentToken != token.token) {
    menuItems = [...menuItems,
      {
        title: "Token löschen",
        subTitle: "Alte Tokens sollten gelöscht werden!",
        icon: {
          name: "delete",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () => deleteToken(token)
      },
    ];
  }

  return (
    <Screen style={styles.screen}>
      <FlatList
        data={menuItems}
        keyExtractor={(menuItem) => menuItem.title}
        ItemSeparatorComponent={ListItemSeparator}
        renderItem={({ item }) => (
          <ListItem
            title={item.title}
            subTitle={item.subTitle}
            IconComponent={
              <Icon
                name={item.icon.name}
                backgroundColor={item.icon.backgroundColor}
              />
            }
            onPress={ 
              item.targetScreen ? () => navigation.navigate(item.targetScreen) : 
              item.onPressCallback? item.onPressCallback : null
            }
            renderChevron={ item.targetScreen ? true : false }
          />
        )}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.light,
  },
  container: {
    margin: 20,
  },
});

export default AccountPushtokenDetailsScreen;
