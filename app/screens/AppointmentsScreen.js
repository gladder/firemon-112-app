import React, { useEffect, useState }  from "react";
import { View, StyleSheet, RefreshControl, SectionList, Text, Pressable } from "react-native";
import AppText from "../components/Text";
import Button from "../components/Button";
import appointments from "../api/appointments";
import appointmentSubscription from "../api/appointmentSubscription";
import FeedbackButton from "../components/FeedbackButton";
import colors from "../config/colors";
import moment from "moment";
import Screen from "../components/Screen";
import useApi from "../hooks/useApi";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import routes from "../navigation/routes";
import Toast from "react-native-root-toast";
import { useStation } from "../auth/stationContext";
import { useIsFocused } from "@react-navigation/native";

function AppointmentsScreen({ navigation }) {
  const [items, setItems] = useState([]);
  const [error, setError] = useState(false);
  const [year, setYear] = useState(moment().year());
  const getAppointmentsApi = useApi(appointments.getAppointments);
  const getAppointmentSubscriptionApi = useApi(appointmentSubscription.subscribe);
  const [hideOutdated, setHideOutdated] = useState(true);
  const { selectedStation } = useStation();
  const isFocused = useIsFocused();

  const increaseYear = () => {
    setYear(year+1);
  }

  const decreaseYear = () => {
    setYear(year-1);
  }

  useEffect(() => {
    loadAppointments();
  }, [year, hideOutdated, selectedStation, isFocused]);

  useEffect(() => {
    if (hideOutdated) {
      navigation.setOptions({
        headerRight: () => (
          <MaterialCommunityIcons name="eye-off" margin={10} color={colors.medium} size={24} onPress={() => {setHideOutdated(false); Toast.show('Alle Termine sichtbar', { duration: Toast.durations.SHORT, });}}/>
        ),
      });
    } else {
      navigation.setOptions({
        headerRight: () => (
          <MaterialCommunityIcons name="eye" margin={10} color={colors.medium} size={24} onPress={() => {setHideOutdated(true); Toast.show('Alte Termine ausgeblendet', { duration: Toast.durations.SHORT, });}}/>
        ),
      });
    }
  }, [navigation, hideOutdated]);

  const loadAppointments = async () => {
    if (!selectedStation || !isFocused) {
      //console.log("STOPPED Calling Appts - no station or not in focus", selectedStation === null, isFocused);
      return;
    }
    //console.log("PERFORM Call Appts");
    const resp = await getAppointmentsApi.request({ year: year, station_id: selectedStation.id });
    let newItems = [];
    if (resp && resp.ok && resp.data && resp.data.success) {
      const itemArray = Object.values(resp.data.data);
      let startI = 0;
      if (year == moment().year() && hideOutdated) {
        startI = moment().month();
      }
      for (let i = startI; i <= 11; i++) {
        const filteredItems = itemArray.filter((item) => moment(item.date_from).month() === i && (moment(item.date_to) > moment() || !hideOutdated ));
        if (filteredItems.length > 0) {
          newItems.push(
            {
              title: MonthNames[i],
              data: filteredItems,
            }
          );
        }
      }
      setError(false);
    } else{
      setError(true);
    }
    setItems(newItems);
  }

  const onRefresh = () => {
    loadAppointments();
  };

  _listEmptyComponent = () => {
    return (
      <AppText style={styles.emptyList}>Es sind keine Termine vohanden</AppText>
    );
  }

  const onSubscribe = async (event_id, available) => {
    await getAppointmentSubscriptionApi.request(event_id, available);
    loadAppointments();
  }

  const MonthNames = [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember"
  ];

  const DayNamesShort = [
    "So",
    "Mo",
    "Di",
    "Mi",
    "Do",
    "Fr",
    "Sa"
  ];

  return (
    <>
      <Screen style={styles.screen}>
        <View style={styles.yearHolder}>
          <MaterialCommunityIcons name="chevron-left" style={styles.yearNaviButton} size={40} onPress={decreaseYear} />
          <AppText style={styles.yearLabel}>{year}</AppText>
          <MaterialCommunityIcons name="chevron-right" style={styles.yearNaviButton} size={40} onPress={increaseYear} />
        </View>
        {error && (
          <>
            <AppText>Es konnten keine Daten empfangen werden.</AppText>
            <Button title="Erneut versuchen" onPress={onRefresh} />
          </>
        )}
        <SectionList
          refreshControl={
            <RefreshControl refreshing={getAppointmentsApi.loading} onRefresh={onRefresh} />
          }
          style={styles.apptHolder}
          sections={items}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => {
            const from = moment(item.date_from);
            const to = moment(item.date_to);
            const day = from.format('D');
            const time = from.format('HH:mm');
            const weekDay = DayNamesShort[from.day()];
            return (
              <View style={styles.item}>
                <View style={styles.itemDate}>
                  <Text style={styles.day}>{day}</Text>
                  <Text style={styles.weekDay}>{weekDay}</Text>
                  <Text style={styles.time}>{time}</Text>
                  {item.subscription_required == 1 && item.subs != null && (
                    <>
                      <Pressable onPress={() => navigation.navigate(routes.APPOINTMENT_SUBSCRIPTION, { appointment:item })}>
                        <Text style={styles.cnt_avail}>{ item.subs.count_available }</Text>
                        <Text style={styles.cnt_avail_label}>zusagen</Text>
                        <Text style={styles.cnt_not_avail}>{ item.subs.count_not_available }</Text>
                        <Text style={styles.cnt_not_avail_label}>absagen</Text>
                      </Pressable>
                    </>
                  )}
                </View>
                <View style={styles.itemDetails}>
                  <Text style={styles.label}>{(item.label ? item.label : "" )}{(item.duty == 1 ? " (Pflicht!)" : "" )}</Text>
                  <Text style={styles.location}>{ (item.category ? item.category+" // " : "" ) }{(item.location ? item.location : "" )}</Text>
                  <Text style={styles.group}>{ (item.group ? item.group : "") }</Text>
                  <Text style={styles.description}>{ (item.description ? item.description : "" )}</Text>
                  <Text>Ende: {to.format("DD.MM.YYYY, HH:mm")} Uhr</Text>
                  {item.subscription_required == 1 && (
                    <>
                    <View style={styles.feedbackHolder}>
                      <View style={styles.feedbackHolderRow}>
                        <FeedbackButton
                          title="Ich bin dabei!"
                          onPress={() => onSubscribe(item.id, 1)}
                          color="feedback_instant"
                          icon="thumb-up"
                          state={ item.sub.available === 1 }
                        />
                        <FeedbackButton
                          title="Keine Zeit"
                          onPress={() => onSubscribe(item.id, 0)}
                          color="feedback_notavail"
                          icon="thumb-down"
                          state={ item.sub.available === 0 }
                        />
                      </View>
                    </View>
                    </>
                  )}
                </View>
              </View>
            );
          }}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.headerHolder}>
              <Text style={styles.header}>{title}</Text>
            </View>
          )}
          ListEmptyComponent={this._listEmptyComponent}
        />
      </Screen>
    </>
  );

}

const styles = StyleSheet.create({
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
  feedbackHolder: {
    marginHorizontal: -5,
    marginBottom: -5,
    marginTop: 5,
    backgroundColor: "white",
    borderRadius: 10,
  },
  feedbackHolderRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  yearHolder: {
    flexDirection: "row",
    backgroundColor: colors.white,
    borderRadius: 15,
    marginBottom: 20
  },
  yearNaviButton: {
    color: colors.primary,
    padding: 10,
  },
  yearLabel: {
    fontWeight: "bold",
    flex: 3,
    textAlign: "center",
    padding: 10,
    fontSize: 24,
    verticalAlign: "middle"
  },
  apptHolder: {
    backgroundColor: colors.white,
    borderRadius: 15,
    flex: 1,
  },
  headerHolder: {
    padding: 15,
    backgroundColor: '#fff',
  },
  header: {
    fontSize: 24,
  },
  emptyList: {
    textAlign: "center",
    paddingVertical: 20
  },
  item: {
    padding: 20,
    marginBottom: 10,
    marginHorizontal: 10,
    backgroundColor: colors.light,
    borderRadius: 5,
    flexDirection: "row"
  },
  day: {
    color: colors.primary,
    fontSize: 32,
    textAlign: "center"
  },
  weekDay: {
    fontSize: 16,
    textAlign: "center"
  },
  time: {
    fontSize: 14,
    textAlign: "center",
    fontWeight: "bold"
  },
  cnt_avail: {
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold",
    color: "green",
    marginTop: 25
  },
  cnt_avail_label: {
    fontSize: 10,
    textAlign: "center",
    fontWeight: "bold",
    color: "green",
  },
  cnt_not_avail: {
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold",
    color: "red",
    marginTop: 5
  },
  cnt_not_avail_label: {
    fontSize: 10,
    textAlign: "center",
    fontWeight: "bold",
    color: "red",
  },
  itemDate: {
    flexDirection: "column",
    paddingRight: 15,
    paddingLeft: 5
  },
  itemDetails: {
    flex: 1
  },
  label: {
    fontSize: 18,
    marginVertical: 5,
    color: colors.primary
  },
  location: {
    fontSize: 16,
  },
  itemSub: {
    flex: 1,
    marginTop: 10
  },
  group: {
    fontSize: 16,
    color: colors.secondary,
    fontWeight: "bold"
  }
});

export default AppointmentsScreen;
