import React, { useEffect, useState }  from "react";
import { View, StyleSheet, RefreshControl, SectionList, Text, Pressable } from "react-native";
import AppText from "../components/Text";
import appointments from "../api/appointments";
import ActivityIndicator from "../components/ActivityIndicator";
import colors from "../config/colors";
import moment from "moment";
import Screen from "../components/Screen";
import useApi from "../hooks/useApi";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Button from "../components/Button";
import { useStation } from "../auth/stationContext";

function AppointmentSubscriptionScreen({ route, navigation }) {
  const [items, setItems] = useState([]);
  const [error, setError] = useState(false);
  const getAppointmentsApi = useApi(appointments.getAppointments);
  const appointment = route.params.appointment;
  const { selectedStation } = useStation();

  useEffect(() => {
    loadSubscriptions();
  }, []);

  const loadSubscriptions = async () => {
    const resp = await getAppointmentsApi.request({year: 0, appointment_id: appointment.id, include_sub_details:1, station_id: selectedStation.id});
    let newItems = [];
    if (resp && resp.ok && resp.data && resp.data.success) {
      if (resp.data.data != null && resp.data.data.length > 0 && resp.data.data[0]["subs"] != null) {
        let subs = resp.data.data[0]["subs"];
        console.log(subs);
        if (subs["available"].length > 0) {
          newItems.push(
            {
              title: "Zugesagt (" + subs["available"].length + ")",
              data: subs["available"],
            }
          );
        }

        if (subs["not_available"].length > 0) {
          newItems.push(
            {
              title: "Abgesagt (" + subs["not_available"].length + ")" ,
              data: subs["not_available"],
            }
          );
        }

        if (subs["no_feedback"].length > 0) {
          newItems.push(
            {
              title: "Keine Rückmeldung (" + subs["no_feedback"].length + ")",
              data: subs["no_feedback"],
            }
          );
        }
        console.log(newItems);
      }
      setError(false);
    } else{
      setError(true);
    }
    setItems(newItems);
  }

  const onRefresh = () => {
    loadSubscriptions();
  };

  _listEmptyComponent = () => {
    return (
      <AppText style={styles.emptyList}>Es sind keine Zu- oder Absagen vohanden</AppText>
    );
  }

  const from = moment(appointment.date_from);

  return (
    <>
      <ActivityIndicator visible={getAppointmentsApi.loading} />
      <Screen style={styles.screen}>
        {error && (
          <>
            <AppText>Es konnten keine Daten empfangen werden.</AppText>
            <Button title="Erneut versuchen" onPress={onRefresh} />
          </>
        )}
        <View style={styles.item}>
          <AppText>{appointment.label}</AppText>
          <AppText>{appointment.label}</AppText>
          <AppText>{from.format("DD.MM.YYYY, HH:mm")}</AppText>
        </View>
        <SectionList
          refreshControl={
            <RefreshControl refreshing={getAppointmentsApi.loading} onRefresh={onRefresh} />
          }
          style={styles.apptHolder}
          sections={items}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => {
            const performed = moment(item.performed);
            const note = item.note;
            const name = item.name;
            let textStyle = styles.label_no_feedback;
            if (item.state == "available") {
              textStyle = styles.label_available;
            }
            if (item.state == "not_available") {
              textStyle = styles.label_not_available;
            }
            return (
              <View style={styles.item}>
                <Text style={[styles.label,textStyle]}>{name}</Text>
                {note.length > 0 && (
                  <>
                  <Text style={styles.note}>{note}</Text>
                  </>
                )}
              </View>
            );
          }}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.headerHolder}>
              <Text style={styles.header}>{title}</Text>
            </View>
          )}
          ListEmptyComponent={this._listEmptyComponent}
        />
      </Screen>
    </>
  );

}

const styles = StyleSheet.create({
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
  apptHolder: {
    backgroundColor: colors.white,
    borderRadius: 15,
    flex: 1
  },
  headerHolder: {
    padding: 15,
    backgroundColor: '#fff',
  },
  header: {
    fontSize: 24,
  },
  emptyList: {
    textAlign: "center",
    paddingVertical: 20
  },
  item: {
    padding: 10,
    marginBottom: 10,
    marginHorizontal: 10,
    backgroundColor: colors.light,
    borderRadius: 5,
    flexDirection: "column"
  },
  itemDetails: {
    flex: 1
  },
  label_available: {
    color: "green",
  },
  label_not_available: {
    color: "red",
  },
  label_no_feedback: {
    color: "gray",
  },
  label: {
    fontSize: 20,
    marginVertical: 5,
    color: colors.primary
  },
  note: {
    fontSize: 16,
  },
});

export default AppointmentSubscriptionScreen;
