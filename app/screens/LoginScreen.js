import React, { useState } from "react";
import { StyleSheet, Image, View } from "react-native";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import * as Yup from "yup";

import Screen from "../components/Screen";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton
} from "../components/forms";
import Text from "../components/Text";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import routes from "../navigation/routes";
import { Platform } from "react-native";

const validationSchema = Yup.object().shape({
  email: Yup.string().required("Angabe notwendig").email("Fehlerhafte E-Mail").label("Email"),
  password: Yup.string().required("Angabe notwendig").min(4,"Passwort ist zu kurz").label("Password"),
});

function LoginScreen({ navigation }) {
  const auth = useAuth();
  const [loginFailed, setLoginFailed] = useState(false);
  const [loginErrorMessage, setLoginErrorMessage] = useState("Login nicht erfolgreich");

  const handleSubmit = async ({ email, password }) => {
    const result = await authApi.login(email, password);
    console.log("Login Response:", result);
    if (!result.ok || !(await auth.logIn(result.data))) {
      setLoginFailed(true);
      console.log('Logino failed', result.data.error);
      if (result && result.data && result.data.error) {
        console.log('Logino failed', result.data.error);
        setLoginErrorMessage(result.data.error);
      }
    } else {
      console.log('Logino success!');
      setLoginFailed(false);
    }
  };

  const onSmsLoginPress = () => {
    navigation.navigate(routes.LOGIN_SMS_INIT);
  };

  return (
    <Screen style={styles.container}>
      
      <KeyboardAwareScrollView
          behavior="position"
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
          keyboardShouldPersistTaps="true"
        >
          <View>
            <Image style={styles.logo} source={require("../assets/logo_red.png")} />

            <Form
              initialValues={{ email: "", password: "" }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <ErrorMessage
                error={loginErrorMessage}
                visible={loginFailed}
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password"
                placeholder="Passwort"
                secureTextEntry
                textContentType="password"
              />
              <SubmitButton title="Login" />
              <Text style={styles.buttonSmsLogin} onPress={onSmsLoginPress}>SMS-Login</Text>
            </Form>
          </View>
        </KeyboardAwareScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  logo: {
    width: 150,
    height: 150,
    alignSelf: "center",
    marginBottom: 20,
  },
  buttonSmsLogin: {
    color: "gray",
    textAlign: "center",
    marginTop: 20,
  },
});

export default LoginScreen;
