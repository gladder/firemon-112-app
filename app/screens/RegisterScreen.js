import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import * as Yup from "yup";

import Screen from "../components/Screen";
import register from "../api/register";
import useApi from "../hooks/useApi";
import AppText from "../components/Text";
import AppButton from "../components/Button";
import { Platform } from "react-native";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import ActivityIndicator from "../components/ActivityIndicator";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Angabe notwendig").label("Vorname"),
  surname: Yup.string().required("Angabe notwendig").label("Nachname"),
  email: Yup.string().required("Angabe notwendig").email("Fehlerhafte E-Mail").label("Email"),
  registration_pin: Yup.number("PIN muss eine Nummer sein").required("Angabe notwendig").label("Wehr-PIN"),
  alert_sms: Yup.string()
    .matches(/^(\+491|491|00491|01){1}[0-9]*$/, "Falsches Rufnummerformat")
    .min(10, "Rufnummer zu kurz")
    .max(16, "Rufnummer zu lang")
    .required("Angabe notwendig")
    .label("Handynummer"),
});

function RegisterScreen({ navigation }) {
  const registerApi = useApi(register.register);
  const [error, setError] = useState();
  const [success, setSuccess] = useState(false);

  const handleSubmit = async ({ name, surname, email, alert_sms, registration_pin }) => {
    const result = await registerApi.request(name, surname, email, alert_sms, registration_pin);

    if (!result || !result.ok || !result.data || !result.data.success) {
      if (result.data && result.data.error) {
        setError(result.data.error);
      } else {
        setError("Ein unerwarteter Fehler ist aufgetreten");
        console.log(result);
      }
      return;
    } else {
      setSuccess(true);
    }
  };

  return (
    <>
      <ActivityIndicator visible={registerApi.loading} />
      <Screen style={styles.container}>
        <KeyboardAwareScrollView
          behavior="position"
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
          keyboardShouldPersistTaps="true"
        >
          {success ?

            <View style={styles.container}>
              <AppText>Die Registrierung war erfolgreich! Du hast eine E-Mail mit einem Aktivierungs-Link und Deinem Initial-Passwort erhalten. Bitte aktiviere den Account und ändere Dein Passwort - dann kannst Du die App sofort nutzen.</AppText>
              <AppButton
                title="zum Login"
                onPress={() => navigation.popToTop()} />
            </View>

            :

            <View>
            <Form
              initialValues={{ name: "", surname: "", email: "", registration_pin: "", alert_sms: "" }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <ErrorMessage error={error} visible={error} />
              <FormField
                autoCorrect={false}
                icon="account"
                name="name"
                placeholder="Vorname"
              />
              <FormField
                autoCorrect={false}
                icon="account"
                name="surname"
                placeholder="Nachname"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="email"
                placeholder="E-Mail Adresse"
                textContentType="emailAddress"
              />
              <FormField
                autoCorrect={false}
                icon="lock-question"
                name="registration_pin"
                keyboardType="number-pad"
                placeholder="Wehr-PIN"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="cellphone"
                keyboardType="phone-pad"
                name="alert_sms"
                placeholder="Handynummer"
                textContentType="telephoneNumber"
              />
              <SubmitButton title="Registrieren" />
            </Form>
            </View>
          }
        </KeyboardAwareScrollView>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default RegisterScreen;
