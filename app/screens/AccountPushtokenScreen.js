import React, { useEffect, useState } from "react";
import { Alert, StyleSheet, View, FlatList } from "react-native";
import { ListItem, ListItemSeparator } from "../components/lists";
import colors from "../config/colors";
import Icon from "../components/Icon";
import Screen from "../components/Screen";
import pushtoken from "../api/pushtoken";
import AppText from "../components/Text";
import * as Notifications from 'expo-notifications';
import routes from "../navigation/routes";
import Constants from 'expo-constants';

function AccountPushtokenScreen({ navigation }) {
  const getPushTokenApi = useApi(pushtoken.getPushTokens);
  const [currentToken, setCurrentToken] = useState("");

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setToken();
      getPushTokenApi.request();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const setToken = async () => {
    let token = null;
    if (__DEV__)  {
      token = (await Notifications.getExpoPushTokenAsync({
        projectId: Constants.expoConfig.extra.eas.projectId,
      }));
    } else {
      token = (await Notifications.getDevicePushTokenAsync());
    }
    if (token != null) {
      setCurrentToken(token.data);
    }
  };

  _listEmptyComponent = () => {
    return (
      <AppText>Es sind keine Push-Tokens vorhanden</AppText>
    )
  }

  return (
    <Screen style={styles.screen}>
      <View style={styles.container}>
        <AppText>Per Klick auf einen Token wird eine Test Notification versendet.</AppText>
      </View>
      <FlatList
        data={getPushTokenApi.data}
        ListEmptyComponent={this._listEmptyComponent}
        keyExtractor={(pushtoken) => pushtoken.id.toString()}
        ItemSeparatorComponent={ListItemSeparator}
        renderItem={({ item }) => (
          <ListItem
            title={item.device_name + (currentToken == item.token ? " (dieses Gerät)" : "")}
            subTitle={item.updated_at + " (" + item.token_type + ")"}
            renderChevron={true}
            activeLineIndicator={currentToken == item.token}
            IconComponent={
              <Icon
                name="bell"
                backgroundColor={colors.primary}
              />
            }
            onPress={() => {
              navigation.navigate(routes.ACCOUNT_PUSHTOKEN_DETAILS, { token:item });
            }}
          />
        )}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.light,
  },
  activeLine: {
    backgroundColor: colors.lightgreen,
  },
  container: {
    margin: 20,
  },
});

export default AccountPushtokenScreen;
