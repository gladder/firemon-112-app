import React, { useEffect, useState, useCallback }  from "react";
import { Alert, StyleSheet, View, FlatList } from "react-native";
import Constants from "expo-constants";
import { useFocusEffect } from '@react-navigation/native';
import { ListItem, ListItemSeparator } from "../components/lists";
import colors from "../config/colors";
import Icon from "../components/Icon";
import routes from "../navigation/routes";
import Screen from "../components/Screen";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import deleteaccount from "../api/deleteaccount";
import * as Notifications from 'expo-notifications';
import * as Linking from 'expo-linking';
import { startActivityAsync, ActivityAction } from 'expo-intent-launcher';
import { Platform } from "react-native";
import { useRingerMode, RINGER_MODE } from 'react-native-ringer-mode';

function AccountScreen({ navigation }) {
  const { user, logOut } = useAuth();
  const deleteUserApi = useApi(deleteaccount.deleteAccount);
  const { mode, error, setMode } = useRingerMode();

  let version = "unknown";
  if (Constants != null && Constants.expoConfig != null && Constants.expoConfig.version != null) {
    version = Constants.expoConfig.version;
  }

  useFocusEffect(
    useCallback(() => {
      // This is called every time the screen comes into focus
      console.log('Screen has come into focus');
      if (checkNotificationStatus('alert')) {
        console.log('notifications fine');
      }
      // The return function is called when the screen goes out of focus
      return () => {
        console.log('Screen has gone out of focus');
      };
    }, [])
  );

  const openAppstore = () => {
    let url;
  
    if (Platform.OS === 'ios') {
      url = 'https://apps.apple.com/de/app/firemon-112-app/id6447294694';
    } else if (Platform.OS === 'android') {
      url = 'https://play.google.com/store/apps/details?id=de.firemon112.app&gl=DE';
    }
  
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
};


  const checkNotificationStatus = async (channelId) => {
    let notificationsOkay = true;
    let dndOkay = true;
    let notificationsHint = '';

    const notificationPermissions = await Notifications.getPermissionsAsync();
    if (!notificationPermissions.granted || notificationPermissions.ios?.status === Notifications.IosAuthorizationStatus.DENIED) {
      notificationsOkay = false;
      notificationsHint = "Aktuell darf die App keine Push Notifications signalisieren.\nBitte gebe diese Einstellung frei und starte die App dann neu.\nIn seltenen Fällen ist eine Neuinstallation der App notwendig.";
    }

    if (notificationsOkay) {
      if (Platform.OS === 'android') {
        const notificationChannel = await Notifications.getNotificationChannelAsync(channelId);
        if (notificationChannel && !notificationChannel.bypassDnd) {
          dndOkay = false;
        }
      }

      if (Platform.OS === 'ios') {
        if (!notificationPermissions.ios?.allowsCriticalAlerts) {
          dndOkay = false;
        }
      }

      if (!dndOkay) {
        notificationsHint = "Aktuell darf die App im 'Bitte nicht Stören Modus' keine Push Notifications signalisieren.\nBitte gebe diese Einstellung frei und starte die App dann neu.\nIn seltenen Fällen ist eine Neuinstallation der App notwendig.";
      }
    }

    if (!notificationsOkay || !dndOkay) {
      // Notification channel exists but does not bypass DND
      Alert.alert('Hinweis zu Push-Notifications', notificationsHint, [
        {
          text: 'Nö!',
          style: 'destructive'
        },
        {
          text: 'Einstellungen', onPress: ( () => {
            Linking.openSettings();
          })
        },
      ]);
    }

    return notificationsOkay && dndOkay;
  };

  const deleteAccount = (() => {
    Alert.alert('Account löschen', 'Willst Du Deinen Account wirklich löschen? Diese Aktion kann nicht rückgängig gemacht werden.', [
      {
        text: 'Nein',
      },
      {text: 'Ja, löschen!', onPress: ( () => {
          deleteUserApi.request();
          logOut();
      })},
    ]);
  });

  const logOutAsk = (() => {
    Alert.alert('Abmelden', 'Willst Du Dich wirklich abmelden? Im Einsatzfall kann die App dann nicht sofort gestartet werden.', [
      {
        text: 'Nein',
      },
      {text: 'Ja, abmelden!', onPress: ( () => {
          logOut();
      })},
    ]);
  });

  const ringerMin = (() => {
    console.log("Ringer Min");
    setMode(RINGER_MODE.silent);
  });

  const ringerMax = (() => {
    console.log("Ringer Max");
    setMode(RINGER_MODE.normal);
  });

  const ringerVibrate = (() => {
    console.log("Ringer Vibrate");
    setMode(RINGER_MODE.vibrate);
  });

  let menuItems = [
    {
      title: user.name + " " + user.surname,
      subTitle: user.email,
      icon: {
        name: "account",
        backgroundColor: colors.primary,
      },
      targetScreen: routes.ACCOUNT_DATA,
    },
    {
      title: "Passwort ändern",
      icon: {
        name: "lock",
        backgroundColor: colors.primary,
      },
      targetScreen: routes.ACCOUNT_PASSWORD,
    },
    {
      title: "Meine Schleifen",
      icon: {
        name: "bell",
        backgroundColor: colors.primary,
      },
      targetScreen: routes.ACCOUNT_ALERTGROUPS,
    },
    {
      title: "Meine Push-Tokens",
      icon: {
        name: "message-badge-outline",
        backgroundColor: colors.primary,
      },
      targetScreen: routes.ACCOUNT_PUSHTOKEN,
    },
    {
      title: user.station.name,
      icon: {
        name: "home",
        backgroundColor: colors.primary,
      },
    },
    {
      title: "Version: " + version,
      icon: {
        name: "information-outline",
        backgroundColor: colors.primary,
      },
      onPressCallback: () => openAppstore()
    },
    {
      title: "Notification Settings",
      icon: {
        name: "cog",
        backgroundColor: colors.secondary,
      },
      onPressCallback: () => Linking.openSettings()
    },
    {
      title: "Abmelden",
      icon: {
        name: "logout",
        backgroundColor: colors.secondary,
      },
      onPressCallback: () => logOutAsk()
    },
    {
      title: "Account löschen",
      icon: {
        name: "trash-can-outline",
        backgroundColor: colors.secondary,
      },
      onPressCallback: () => deleteAccount()
    }
  ];

  if (Platform.OS === 'android') {
    menuItems = [...menuItems, 
      {
        title: "Zugriff bei Bitte nicht Stören",
        subTitle: "Zugriff bitte gestatten",
        icon: {
          name: "bell",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () =>startActivityAsync(ActivityAction.NOTIFICATION_POLICY_ACCESS_SETTINGS)
      }, 
      {
        title: "Battery Optimierung",
        subTitle: "Bitte von Akku-Optimierung ausschließen",
        icon: {
          name: "battery-high",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () =>startActivityAsync(ActivityAction.IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
      },
      {
        title: "Ringer Vol Min",
        subTitle: "Setzt Ringer Volume auf Min (für Testzwecke)",
        icon: {
          name: "volume-off",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () => ringerMin()
      },
      {
        title: "Ringer Vol MAX",
        subTitle: "Setzt Ringer Volume auf max (für Testzwecke)",
        icon: {
          name: "volume-high",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () => ringerMax()
      },
      {
        title: "Ringer Vibrate",
        subTitle: "Setzt Ringer auf Vibtation (für Testzwecke)",
        icon: {
          name: "volume-vibrate",
          backgroundColor: colors.secondary,
        },
        onPressCallback: () => ringerVibrate()
      },
    ];
  }

  return (
    <Screen style={styles.screen}>
      <FlatList
        data={menuItems}
        keyExtractor={(menuItem) => menuItem.title}
        ItemSeparatorComponent={ListItemSeparator}
        renderItem={({ item }) => (
          <ListItem
            title={item.title}
            subTitle={item.subTitle}
            IconComponent={
              <Icon
                name={item.icon.name}
                backgroundColor={item.icon.backgroundColor}
              />
            }
            onPress={ 
              item.targetScreen ? () => navigation.navigate(item.targetScreen) : 
              item.onPressCallback? item.onPressCallback : null
            }
            renderChevron={ item.targetScreen ? true : false }
          />
        )}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.light,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
});

export default AccountScreen;
