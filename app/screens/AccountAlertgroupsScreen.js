import React, { useEffect, useState } from "react";
import { StyleSheet, View, FlatList, Text } from "react-native";
import { ListItem, ListItemSeparator } from "../components/lists";
import colors from "../config/colors";
import Icon from "../components/Icon";
import Screen from "../components/Screen";
import alertgroups from "../api/alertgroups";
import AppText from "../components/Text";
import authStorage from "../auth/storage";

function AccountAlertgroupsScreen() {
  const getAlertGroupsApi = useApi(alertgroups.getAlertGroups);
  const [items, setItems] = useState([]);

  useEffect(() => {
    loadItems();
  }, []);

  const loadItems = async () => {
    const station = await authStorage.getFiremonStation();
    const linkedStations = await authStorage.getFiremonLinkedStations();

    const resp = await getAlertGroupsApi.request();
    if (resp && resp.ok && resp.data && resp.data.success) {
      setItems(processData(resp.data.data, station, linkedStations));
    }
  }

  const processData = (data, station, linkedStations) => {
    const sortedData = data.sort((a, b) => a.station_id - b.station_id);
    const groupedData = [];
    let lastStationId = 0;
  
    sortedData.forEach(item => {
      if (item.station_id !== lastStationId) {
        let stationName = "unbekannt";
        if (station && item.station_id == station.id) {
          stationName = station.name;
        } else if (linkedStations) {
          let s = linkedStations.find(v => v.id === item.station_id);
          if (s) {
            stationName = s.name;
          }
        }
        
        groupedData.push({
          type: 'header',
          stationId: item.station_id,
          stationName: stationName,
          key: `header-${item.station_id}`
        });
        lastStationId = item.station_id;
      }
      groupedData.push({...item, key: `item-${item.id}`});
    });
  
    return groupedData;
  };

  const renderItem = ({ item }) => {
    if (item.type === 'header') {
      return (
        <Text style={styles.headerStyle}>{ item.stationName }</Text>
      );
    } else {
      return (
        <ListItem
          title={item.label}
          subTitle={`Trigger: ${item.trigger_alert_email ? "E-Mail " : ""}${item.trigger_alert_sms ? "SMS " : ""}${item.trigger_alert_call ? "Anruf " : ""}${item.trigger_alert_app ? "App " : ""}`}
          renderChevron={false}
          IconComponent={
            <Icon
              name="bell"
              backgroundColor={item.enabled ? colors.primary : colors.medium}
            />
          }
        />
      );
    }
  };

  _listEmptyComponent = () => {
    return (
      <AppText>Es sind keine Schleifen zugewiesen</AppText>
    )
  }

  return (
    <Screen style={styles.screen}>
      <FlatList
        data={items}
        ListEmptyComponent={this._listEmptyComponent}
        keyExtractor={(item) => item.key}
        ItemSeparatorComponent={ListItemSeparator}
        renderItem={renderItem}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.light,
  },
  headerStyle: {
    fontSize: 18,
    padding: 10,
    margin: 5,
    color: 'black'
  },
});

export default AccountAlertgroupsScreen;
