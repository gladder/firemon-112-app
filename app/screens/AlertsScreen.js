import React, { useEffect, useState, useCallback } from "react";
import { RefreshControl, FlatList, StyleSheet, AppState } from "react-native";
import { useFocusEffect } from '@react-navigation/native';
import ActivityIndicator from "../components/ActivityIndicator";
import Button from "../components/Button";
import AlertCard from "../components/AlertCard";
import colors from "../config/colors";
import alertsApi from "../api/alerts";
import routes from "../navigation/routes";
import Screen from "../components/Screen";
import AppText from "../components/Text";
import useApi from "../hooks/useApi";
import Toast from "react-native-root-toast";

function AlertsScreen({ navigation }) {
  const getAlertsApi = useApi(alertsApi.getAlerts);
  //const [aState, setAppState] = useState(AppState.currentState);
  const { user } = useAuth();

  useFocusEffect(
    useCallback(() => {
      // This is called every time the screen comes into focus
      console.log('Screen has come into focus');
      getAlertsApi.request();
      // The return function is called when the screen goes out of focus
      return () => {
        console.log('Screen has gone out of focus');
      };
    }, [])
  );

  //useEffect(() => {
  //  getAlertsApi.request();
  //}, [aState]);

  const onRefresh = () => {
    getAlertsApi.request();
  };

  _listEmptyComponent = () => {
      return (
        <AppText>Es sind keine Alarme vohanden</AppText>
      )
  }

  const openAlert = (alert) => {
    console.log("Open Alert user", user);
    console.log("Open Alert alert", alert);
    let station = user.station; // Old Handling - keep for legacy
    if (alert.station !== null) {
      station = alert.station; // New Handling considers station that got alert (Zweitwehr usw)
    }
    let userCanOpen = !station.alert_details_protected; // this is a little weird written to still interface old apis - may be refactured after may 2024
    if (!userCanOpen && alert.mission_admin != null && alert.mission_operator != null) {
      userCanOpen = alert.mission_admin || alert.mission_operator;
    }
    if (userCanOpen) {
      navigation.navigate(routes.ALERT_DETAILS, { alert:alert });
    } else {
      Toast.show('Der Zugriff auf Einsatz-Details ist vom Wehr-Administrator auf Führungskräfte beschränkt worden.', {
        duration: Toast.durations.SHORT,
        position: Toast.positions.CENTER
      });
    }
  };

  return (
    <>
      <Screen style={styles.screen}>
        {getAlertsApi.error && (
          <>
            <AppText>Es konnten keine Daten empfangen werden. { getAlertsApi.error ? getAlertsApi.errorMessage : "" }</AppText>
            <Button title="Erneut versuchen" onPress={onRefresh} />
          </>
        )}
        <FlatList
          refreshControl={
            <RefreshControl refreshing={getAlertsApi.loading} onRefresh={onRefresh} />
          }
          data={getAlertsApi.data}
          ListEmptyComponent={this._listEmptyComponent}
          keyExtractor={(alert) => alert.id.toString()}
          renderItem={({ item }) => (
            <AlertCard
              alert={item}
              onPress={() => openAlert(item)}
              onFeedback={() => onRefresh() }
            />
          )}
        />
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
});

export default AlertsScreen;
