import React, { useEffect, useState }  from "react";
import {
  View,
  StyleSheet,
  Platform,
} from "react-native";
import colors from "../config/colors";
import { WebView } from 'react-native-webview';
import settings from "../config/settings";
import authStorage from "../auth/storage";
import alertFeedback from "../api/alertFeedback";
import Toast from "react-native-root-toast";
import * as Linking from 'expo-linking';

function AlertDetailsScreen({ route, navigation }) {
  const [url, setUrl] = useState('');
  const getAlertFeedbackApi = useApi(alertFeedback.feedback);
  const { user } = useAuth();
  
  // TODO: in future there will be no alert object anymore - deal with it
  const alert = route.params.alert;
  const feedback_action = route.params.actionIdentifier;

  useEffect(() => {
    if (alert && feedback_action != null) {
      console.log("SEND FEEDBACK ", feedback_action);
      let feedback = 0;
      if (feedback_action == "feedback_i") {
        feedback = 1;
      }
      if (feedback_action == "feedback_d") {
        feedback = 2;
      }
      if (feedback_action == "feedback_l") {
        feedback = 3;
      }
      if (feedback_action == "feedback_n") {
        feedback = 4;
      }
      if (feedback > 0) {
        getAlertFeedbackApi.request(alert.id, feedback);
      }
    }
  }, []);

  useEffect(() => {
    const fetchUrlAsync = async () => {
      let station = user.station; // Old Handling - keep for legacy
      if (alert.station !== null) {
        station = alert.station; // New Handling considers station that got alert (Zweitwehr usw)
      }
      let userCanOpen = !station.alert_details_protected; // this is a little weird written to still interface old apis - may be refactured after may 2024
      if (!userCanOpen && alert.mission_admin != null && alert.mission_operator != null) {
        userCanOpen = alert.mission_admin || alert.mission_operator;
      }
      if (userCanOpen) {
        const token = await authStorage.getToken();
        setUrl(settings.getCurrentMonitorUrl().monitorUrl + "?injectToken="+token+"&injectAlert="+alert.id+"&injectClientType="+Platform.OS);
      } else {
        navigation.goBack();
        Toast.show('Der Zugriff auf Einsatz-Details ist vom Administrator auf Führungskräfte beschränkt worden.', {
          duration: Toast.durations.SHORT,
          position: Toast.positions.CENTER
        });
      }
    };

    fetchUrlAsync();
  }, [alert]);

  const handleShouldStartLoadWithRequest = request => {
    if (request.url.endsWith('.pdf') || request.url.endsWith('.jpg') || request.url.endsWith('.jpeg') || request.url.endsWith('.png')) {
      // If the link is a PDF or an image, open it with
      // the device's default PDF reader or image viewer
      Linking.openURL(request.url);
      return false;
    }
    // For other links, let the WebView handle them
    return true;
  };

  const WebViewComponent = ( { url }) => {
    return (
      <WebView
            style={styles.webviewContainer}
            source={{uri:  url}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            onShouldStartLoadWithRequest={handleShouldStartLoadWithRequest}
            cacheEnabled={false}
          />
    );
  };

  return (
      <View style={styles.detailsContainer}>
        <View style={styles.webviewHolder}>
          {url && <WebViewComponent url={url} />}
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  webviewContainer: {
    flex: 1
  },
  webviewHolder: {
    backgroundColor: "black",
    flex: 1
  },
  detailsContainer: {
    flex: 1
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
    backgroundColor: "green"
  },
  text: {
    color: colors.medium,
    fontWeight: "bold",
  },
  title: {
    fontSize: 24,
    fontWeight: "500",
    backgroundColor: "red"
  },
});

export default AlertDetailsScreen;
