import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import * as Yup from "yup";

import Screen from "../components/Screen";
import smsLoginApi from "../api/smsLogin";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import useAuth from "../auth/useAuth";
import { Platform } from "react-native";

const validationSchema = Yup.object().shape({
  mobile: Yup.string().required().label("Handynummer")
});

function LoginSmsInitScreen({ navigation }) {
  const [error, setError] = useState();
  const [otpVisible, setOtpVisible] = useState(false);
  const auth = useAuth();

  const handleSubmit = async ({ mobile, otp }) => {
      const res = await smsLoginApi.smslogin(mobile, otp);
      if (!res.ok || (res.data && !res.data.success)) {
        if (res.data && res.data.error) {
          setError(res.data.error);
        } else {
          setError("Unexpected API error");
        }
        if (res.data && res.data.pendingOtp) {
          setOtpVisible(true);
        }
        return;
      }
      setError("");

      if (res.data && res.data.pendingOtp) {
        // Ask for OTP
        setOtpVisible(true);
      } else {
        // Login ok
        if (!auth.logIn(res.data)) {
          setError("Login Failed");
        }
      }
  };

  return (
    <>
      <Screen style={styles.container}>
      <KeyboardAwareScrollView
          behavior="position"
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
          keyboardShouldPersistTaps="true"
        >
          <View>
          <Form
            initialValues={{ mobile: "" }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <ErrorMessage error={error} visible={error} />
            
            <FormField
              autoCorrect={false}
              icon="cellphone"
              name="mobile"
              inputMode="tel"
              placeholder="Handynummer"
              readOnly={otpVisible}
            />
            {otpVisible ?
            <FormField
              autoCorrect={false}
              icon="lock"
              name="otp"
              inputMode="numeric"
              placeholder="Einmalpasswort"
            /> 
            : null}
            <SubmitButton title={ otpVisible ? "Login" : "Passwort anfordern" } />
            
          </Form>
          </View>
          </KeyboardAwareScrollView>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default LoginSmsInitScreen;
