import client from "./client";

const endpoint = "/pushtoken";

const getPushTokens = () => client.get(endpoint);

export default {
  getPushTokens,
};
