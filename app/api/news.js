import client from "./client";

const endpoint = "/news";

const getNews = ({station_id=0}) => client.get(`${endpoint}?station_id=${encodeURIComponent(station_id)}`);

export default {
  getNews,
};
