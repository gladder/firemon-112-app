import client from "./client";

const endpoint = "/delete-account";

const deleteAccount = () => client.post(endpoint);

export default {
  deleteAccount,
};
