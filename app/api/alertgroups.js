import client from "./client";

const endpoint = "/alert-groups";

const getAlertGroups = () => client.get(endpoint);

export default {
  getAlertGroups,
};
