import client from "./client";

const register = (name, surname, email, alert_sms, registration_pin) => client.post("/register", { name, surname, email, alert_sms, registration_pin });

export default {
  register,
};