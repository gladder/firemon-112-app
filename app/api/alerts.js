import client from "./client";

const endpoint = "/recent-alerts";

const getAlerts = () => client.get(endpoint);

export default {
  getAlerts,
};
