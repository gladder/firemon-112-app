import client from "./client";

const smslogin = (phone, otp) => client.post("/smslogin", { phone, otp });

export default {
  smslogin,
};
