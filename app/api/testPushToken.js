import client from "./client";

const testPushToken = (token_id, delay_minutes = 0) =>
  client.post("/test-pushtoken", { 
    token_id: token_id,
    delay_minutes: delay_minutes
  });

export default {
  testPushToken,
};
