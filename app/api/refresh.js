import client from "./client";

const refresh = () => client.post("/refresh");

export default {
  refresh,
};
