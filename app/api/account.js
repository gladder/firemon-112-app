import client from "./client";

const account = (name, surname, email, alert_sms, alert_call, alert_call_second, alert_email) => 
  client.post("/account", { name, surname, email, alert_sms, alert_call, alert_call_second, alert_email });

const password = (password, password_confirmation) => 
  client.post("/password", { password, password_confirmation });

export default {
  account,
  password
};