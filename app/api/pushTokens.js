import client from "./client";

const register = (token, token_type, device_name) =>
  client.post("/pushtoken", { 
    token: token,
    token_type: token_type,
    device_name: device_name
  });

const deleteToken = (token, token_type, device_name) =>
  client.delete("/pushtoken", { 
    token: token,
    token_type: token_type,
    device_name: device_name
  });

export default {
  register,
  deleteToken
};
