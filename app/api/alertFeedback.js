import client from "./client";

const feedback = (alert_id, feedback) => client.post("/alert-feedback", { 
  alert_id: alert_id,
  feedback: feedback
});

export default {
  feedback,
};
