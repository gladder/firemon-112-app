import client from "./client";

/**
 * 
 * @param {'year'} params 
 * @returns 
 */
const getAppointments = ({year, month=0, appointment_id=0, include_sub_details=0, station_id=0}) => {
  let params = {
    year: year
  };

  if (month > 0) {
    params.month = month;
  }

  if (appointment_id > 0) {
    params.appointment_id = appointment_id;
  }

  if (include_sub_details > 0) {
    params.include_sub_details = include_sub_details;
  }

  if (station_id > 0) {
    params.station_id = station_id;
  }

  return client.post("/appointments", params);
}


export default {
  getAppointments,
};
