import client from "./client";

const subscribe = (event_id, available) => client.post("/appointment-subscribe", { 
  event_id: event_id,
  available: available
});

export default {
  subscribe,
};
