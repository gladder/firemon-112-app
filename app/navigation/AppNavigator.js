import React, { useCallback, useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import AccountNavigator from "./AccountNavigator";
import useNotifications from "../hooks/useNotifications";
import AlertNavigator from "./AlertNavigator";
import routes from "./routes";
import AppointmentNavigator from "./AppointmentNavigator";
import NewsNavigator from "./NewsNavigator";

const Tab = createBottomTabNavigator();

const AppNavigator = () => {
  useNotifications(true, true);

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={routes.ALERTS_NAVIGATOR}
        component={AlertNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="fire" color={color} size={32} />
          ),
          headerShown: false,
          title: "Alarme"
        }}
      />
      <Tab.Screen
        name={routes.APPOINTMENT_NAVIGATOR}
        component={AppointmentNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="calendar-month" color={color} size={32} />
          ),
          headerShown: false,
          title: "Termine"
        }}
      />
      <Tab.Screen
        name="News"
        component={NewsNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="newspaper-variant-outline" color={color} size={32} />
          ),
          headerShown: false,
          title: "News"
        }}
      />
      <Tab.Screen
        name="Mein Account"
        component={AccountNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={32} />
          ),
          headerShown: false,
          title: "Mein Account"
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
