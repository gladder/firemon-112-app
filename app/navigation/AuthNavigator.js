import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import WelcomeScreen from "../screens/WelcomeScreen";
import LoginSmsInitScreen from '../screens/LoginSmsInitScreen';

const Stack = createStackNavigator();

const AuthNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Welcome"
      component={WelcomeScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen name="Login" component={LoginScreen} />
    <Stack.Screen name="Register" component={RegisterScreen}  options={ { title: "Registrieren" } }/>
    <Stack.Screen name="LoginSmsInit" component={LoginSmsInitScreen} options={ { title: "Login per SMS" } } />
  </Stack.Navigator>
);

export default AuthNavigator;
