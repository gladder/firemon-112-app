import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AppointmentsScreen from "../screens/AppointmentsScreen";
import AppointmentSubscriptionScreen from "../screens/AppointmentSubscriptionScreen";
import routes from "./routes";
import StationSwitcher from "../components/StationSwitcher";

const Stack = createStackNavigator();

const AppointmentNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: true, presentation: "card",  }}>
    <Stack.Screen name={routes.APPOINTMENTS} component={AppointmentsScreen} options={{ 
      headerTitle: (props) => <StationSwitcher {...props} />, // Set the header title to be the switcher
      headerTitleAlign: 'center'
    }} />
    <Stack.Screen name={routes.APPOINTMENT_SUBSCRIPTION} component={AppointmentSubscriptionScreen} options={ ({route}) => ({ title: "Teilnahme " + route.params.appointment.label }) } />
  </Stack.Navigator>
);

export default AppointmentNavigator;