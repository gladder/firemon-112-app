import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AccountScreen from "../screens/AccountScreen";
import AccountDataScreen from "../screens/AccountDataScreen";
import AccountAlertgroupsScreen from "../screens/AccountAlertgroupsScreen";
import AccountPushtokenScreen from "../screens/AccountPushtokenScreen";
import AccountPasswordScreen from "../screens/AccountPasswordScreen";
import routes from "./routes";
import AccountPushtokenDetailsScreen from "../screens/AccountPushtokenDetailsScreen";

const Stack = createStackNavigator();

const AccountNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name={routes.ACCOUNT_INDEX} component={AccountScreen} options={ { title: "Mein Account" } } />
    <Stack.Screen name={routes.ACCOUNT_PASSWORD} component={AccountPasswordScreen} options={ { title: "Passwort ändern" } } />
    <Stack.Screen name={routes.ACCOUNT_DATA} component={AccountDataScreen} options={ { title: "Meine Daten" } } />
    <Stack.Screen name={routes.ACCOUNT_ALERTGROUPS} component={AccountAlertgroupsScreen} options={ { title: "Meine Schleifen" } } />
    <Stack.Screen name={routes.ACCOUNT_PUSHTOKEN} component={AccountPushtokenScreen} options={ { title: "Meine Push-Tokens" } } />
    <Stack.Screen name={routes.ACCOUNT_PUSHTOKEN_DETAILS} component={AccountPushtokenDetailsScreen} options={ ({route}) => ({ title: "Token für " + route.params.token.device_name }) }  />
  </Stack.Navigator>
);

export default AccountNavigator;
