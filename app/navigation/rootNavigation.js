import React from "react";

let pendingNavigationRoute = null;
const navigationRef = React.createRef();

const navigate = (name, params) => {
  if (navigationRef.current?.isReady() && routeExisting(name)) {
    //console.log("### NAVIGATION ###", "navigate", name, params);
    navigationRef.current?.navigate(name, params);
  } else {
    //console.log("### NAVIGATION ###", "schedule pending navigation", name, params);
    pendingNavigationRoute = { name: name, params: params }
  }
}

function routeExisting(name) {
  const navigationState = navigationRef.current?.getRootState();

  let found = false;
  navigationState.routes.forEach(route => {
    if (route.name == name) {
      found = true;
    }
  });
  return found;
}

const handlePendingNavigation = () => {
  if (pendingNavigationRoute != null) {
    if (navigationRef.current?.isReady() && routeExisting(pendingNavigationRoute.name)) {
      navigationRef.current?.navigate(pendingNavigationRoute.name, pendingNavigationRoute.params);
      pendingNavigationRoute = null;
      //console.log("### NAVIGATION ###", "handled pending navigation");
    } else {
      //console.log("### NAVIGATION ###", "pending navigation but navigation not ready!");
    }
    
  } else {
    //console.log("### NAVIGATION ###", "no pending navigation");
  }
}

export default {
  navigationRef,
  navigate,
  handlePendingNavigation
};
