import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AlertsScreen from "../screens/AlertsScreen";
import AlertDetailsScreen from "../screens/AlertDetailsScreen";

const Stack = createStackNavigator();

const AlertNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: true, presentation: "card",  }}>
    <Stack.Screen name="Alerts" component={AlertsScreen} options={ { title: "Letzte Alarme" } } />
    <Stack.Screen name="AlertDetails" component={AlertDetailsScreen} options={ ({route}) => ({ title: "Einsatz " + route.params.alert.Einsatzstichwort }) } />
  </Stack.Navigator>
);

export default AlertNavigator;