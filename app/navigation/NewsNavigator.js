import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import NewsScreen from "../screens/NewsScreen";
import routes from "./routes";
import StationSwitcher from "../components/StationSwitcher";

const Stack = createStackNavigator();

const NewsNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: true, presentation: "card",  }}>
    <Stack.Screen name={routes.NEWS_INDEX} component={NewsScreen} options={{ 
      headerTitle: (props) => <StationSwitcher {...props} />, // Set the header title to be the switcher
      headerTitleAlign: 'center'
    }} />
  </Stack.Navigator>
);

export default NewsNavigator;