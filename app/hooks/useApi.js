import { useState } from "react";

export default useApi = (apiFunc) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const request = async (...args) => {
    setLoading(true);
    const response = await apiFunc(...args);

    if (response && response.ok && response.data && response.data.success) {
      setError(false);
      setData(response.data.data);
    } else {
      console.log("resp not ok", response);
      if (response && response.data && response.data.error) {
        setErrorMessage(response.data.error);
      } else {
        setErrorMessage("Unexpected API Error");
      }
      setError(true);
    }
    setLoading(false);
    return response;
  };

  return { data, error, errorMessage, loading, request };
};
