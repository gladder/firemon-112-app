import { useEffect } from "react";
import * as Notifications from 'expo-notifications';
import pushTokensApi from "../api/pushTokens";
import * as Device from 'expo-device';
import useAuth from "../auth/useAuth";
import Toast from 'react-native-root-toast';
import { Platform } from "react-native";
import Constants from 'expo-constants';

// TODO: Move this to App.js and add a flag that will take care of permissions on app startup but will post the push token only if user is logged in,...
export default useNotifications = (registerToken = false, registerAlertCategory = false) => {
  let user = null;
  if (registerToken || registerAlertCategory) {
    user = useAuth().user;
  } 
  useEffect(() => {
    registerForPushNotifications(registerToken, registerAlertCategory);
  }, []);

  const registerForPushNotifications = async (registerToken, registerAlertCategory) => {
    try {
      
      console.log("platform:", Platform.OS);

      Notifications.setNotificationHandler({
        handleNotification: async () => ({
          shouldShowAlert: true,
          shouldPlaySound: true,
          shouldSetBadge: true,
        }),
      });

      if (registerAlertCategory && user !== null && user.station !== null) {
        let actions = [];
        if (user.station.alert_feedback_type > 0) {
          actions.push({
            buttonTitle: "Sofort",
            identifier: "feedback_i",
            options: {
              opensAppToForeground: true,
            }
          });
          actions.push({
            buttonTitle: "Nicht verfügbar",
            identifier: "feedback_n",
            options: {
              opensAppToForeground: true
            }
          },);
        }

        if (user.station.alert_feedback_type == 2 || user.station.alert_feedback_type == 4) {
          actions.push({
            buttonTitle: "Verspätet",
            identifier: "feedback_l",
            options: {
              opensAppToForeground: true
            }
          });
        }

        if (user.station.alert_feedback_type == 3) {
          actions.push({
            buttonTitle: "Direkt",
            identifier: "feedback_d",
            options: {
              opensAppToForeground: true
            }
          });
        }

        if (actions.length > 0) {
          await Notifications.setNotificationCategoryAsync(
            "alert",
            actions,
            {
              allowAnnouncement: true,
              allowInCarPlay: true,
              categorySummaryFormat: "Alarme!",
              showSubtitle: true,
              showTitle: true
            }
          );
        }
      }
      
      if (Platform.OS === 'android') {
        await Notifications.setNotificationChannelAsync('alert', {
          name: 'Einsatz Notifications',
          importance: Notifications.AndroidImportance.MAX,
          enableVibrate: true,
          vibrationPattern: [0, 2500, 250, 2500],
          bypassDnd: true,
          enableLights: true,
          lockscreenVisibility: Notifications.AndroidNotificationVisibility.PUBLIC,
          showBadge: true,
          lightColor: '#FF231F7C',
          sound: 'sirene.wav',
        });
        await Notifications.setNotificationChannelAsync('event', {
          name: 'Termine',
          importance: Notifications.AndroidImportance.DEFAULT,
        });
        await Notifications.setNotificationChannelAsync('news', {
          name: 'Neuigkeiten',
          importance: Notifications.AndroidImportance.DEFAULT,
        });
        await Notifications.setNotificationChannelAsync('training', {
          name: 'Lehrgänge',
          importance: Notifications.AndroidImportance.HIGH,
        });
        console.log("channel created");
      }

      if (Device.isDevice) {
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
          console.log("asking for permission");
          const { status } = await Notifications.requestPermissionsAsync({
            ios: {
              allowAlert: true,
              allowBadge: true,
              allowSound: true,
              allowAnnouncements: true,
              allowCriticalAlerts: true,
            }
          });
          finalStatus = status;
        }
        if (finalStatus !== 'granted') {
          Toast.show('Push-Token konnte nicht erzeugt werden', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.CENTER
          });
          return;
        }

        Notifications.setBadgeCountAsync(0);
        if (registerToken) {
          let token = null;
          if (__DEV__)  {
            token = (await Notifications.getExpoPushTokenAsync({
              projectId: Constants.expoConfig.extra.eas.projectId,
            }));
          } else {
            token = (await Notifications.getDevicePushTokenAsync());
          }

          console.log("Token:", token);
          if (token != null) {
            pushTokensApi.register(
              token.data,
              token.type,
              Device.deviceName ? Device.deviceName : 'unknown device'
            );
            
            //Toast.show('Erfolgreich für Push-Nachrichten registriert', {
            //  duration: Toast.durations.SHORT,
            //  position: Toast.positions.CENTER
            //});
          }
        }
      } else {
        Toast.show('Push Nachrichten in Emulatoren nicht möglich.', {
          duration: Toast.durations.SHORT,
          position: Toast.positions.CENTER
        });
      }
      
    } catch (error) {
      console.log('error getting push token', error);
    } 
  }
}
